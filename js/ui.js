function setup() {
    const voices = getVoices();
    const voiceSelect = document.getElementById("voice");
    let options = "<option>Select One</option>";
    for (let i = 0; i < voices.length; i++) {
        options += "<option>" + voices[i].name + "</option>";
    }
    voiceSelect.innerHTML = options;
}

function validateIntegerField(inputField) {
    const startingNumberField = document.getElementById("startingNumber");
    const endingNumberField = document.getElementById("endingNumber");
    const pauseField = document.getElementById("pause");

    const allInputIsGood = $.isNumeric(startingNumberField.value) && $.isNumeric(endingNumberField.value) && $.isNumeric(pauseField.value);
    if (!$.isNumeric(inputField.value)) {
        inputField.style.backgroundColor = "#f08080";
    } else {
        inputField.style.backgroundColor = null;
    }

    if (allInputIsGood) {
        document.getElementById("run").removeAttribute("disabled");
    } else {
        document.getElementById("run").setAttribute("disabled", "disabled");
    }
}

function onRunClicked() {
    const voiceSelect = document.getElementById("voice");
    const startingNumber = parseInt(document.getElementById("startingNumber").value);
    const endingNumber = parseInt(document.getElementById("endingNumber").value);
    const pauseSeconds = parseInt(document.getElementById("pause").value);
    const leftSideField = document.getElementById("leftSide");
    const randomField = document.getElementById("random");
    const allNumbersDiv = document.getElementById("allNumbersDiv");
    const currentNumberDiv = document.getElementById("currentNumber");

    let currentIndex = -1;
    let numbersArray = [];
    let oneStepsHtml = "";

    let sideStrategy = new RightSideOnlyStrategy();
    if (leftSideField.checked) {
        sideStrategy = new BothSidesStrategy();
    }

    let numberOfElements = sideStrategy.getNumberOfElements(startingNumber, endingNumber);
    let indexFinder = new SequentialIndexFinder();
    if (randomField.checked) {
        indexFinder = new RandomIndexFinder(numbersArray, numberOfElements);
    }


    sideStrategy.execute(function (side) {
        for (let i = startingNumber; i <= endingNumber; i++) {
            currentIndex = indexFinder.getIndex(currentIndex);
            numbersArray[currentIndex] = {
                oneStep: i,
                side: side
            };
            oneStepsHtml += "<span data-index=\"" + currentIndex + "\">" + sideStrategy.getText(i, side) + "</span>";
            console.debug("setting numbersArray[" + currentIndex + "] = { oneStep: " + i + ", side: \"" + side + "\" }");
        }
    });
    allNumbersDiv.innerHTML = oneStepsHtml;

    currentIndex = 0;
    let speechStrategy = getSpeechStrategy(voiceSelect);
    let intervalEventHandler = function () {
        let currentSpan = $("span[data-index='" + currentIndex + "'");
        currentSpan.addClass("finished");
        currentNumberDiv.innerHTML = currentSpan.html();
        currentIndex++;
        speechStrategy.speak(currentSpan.html(), function () {
            if (currentIndex < numbersArray.length) {
                setTimeout(intervalEventHandler, pauseSeconds * 1000);
            }
        });
    };
    setTimeout(intervalEventHandler, pauseSeconds * 1000);
}

function getRandomInt(max) {
    return Math.floor(Math.random() * max);
}

let RandomIndexFinder = function (numbersArray, endIndex) {
    this.numbersArray = numbersArray;
    this.endIndex = endIndex;
};

RandomIndexFinder.prototype.getIndex = function () {
    let currentIndex;
    do {
        currentIndex = getRandomInt(this.endIndex);
        console.debug("numbersArray[" + currentIndex + "] = " + this.numbersArray[currentIndex]);
    } while (this.numbersArray[currentIndex] != null);
    return currentIndex;
};

let SequentialIndexFinder = function () {
};

SequentialIndexFinder.prototype.getIndex = function (currentIndex) {
    return currentIndex + 1;
};

let RightSideOnlyStrategy = function () {
};

RightSideOnlyStrategy.prototype.getNumberOfElements = function (startingNumber, endingNumber) {
    return endingNumber - startingNumber + 1;
};

RightSideOnlyStrategy.prototype.getText = function (currentNumber) {
    return currentNumber;
};

RightSideOnlyStrategy.prototype.execute = function (logicToRun) {
    logicToRun("right");
};

let BothSidesStrategy = function () {
};

BothSidesStrategy.prototype.getNumberOfElements = function (startingNumber, endingNumber) {
    return (endingNumber - startingNumber + 1) * 2;
};

BothSidesStrategy.prototype.getText = function (currentNumber, side) {
    if (side.toLowerCase() == "left") {
        return "Left Side: " + currentNumber;
    }
    return "Right Side: " + currentNumber;
};

BothSidesStrategy.prototype.execute = function (logicToRun) {
    logicToRun("left");
    logicToRun("right");
};

let NoVoiceSpeechStrategy = function () {
};

NoVoiceSpeechStrategy.prototype.speak = function (text, onAfterSpeaking) {
    onAfterSpeaking();
};

let UserSelectedVoiceSpeechStrategy = function (voice) {
    this.voice = voice;
};

UserSelectedVoiceSpeechStrategy.prototype.speak = function (text, onAfterSpeaking) {
    let utterThis = new SpeechSynthesisUtterance(text);
    utterThis.onend = onAfterSpeaking;
    window.speechSynthesis.speak(utterThis);
};


function getSelectedVoice(voiceSelect) {
    let voices = getVoices();
    let voiceName = voiceSelect.selectedOptions[0].value;
    for (let i = 0; i < voices.length; i++) {
        if (voices[i].name == voiceName) {
            return voices[i];
        }
    }
    return null;
}

function getSpeechStrategy(voiceSelect) {
    let voice = getSelectedVoice(voiceSelect);
    if (voice == null) {
        return new NoVoiceSpeechStrategy();
    }
    return new UserSelectedVoiceSpeechStrategy(voice);
}
